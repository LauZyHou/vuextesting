import Vue from 'vue';
import App from './App.vue';
import {store} from './store/store';

new Vue({
  store:store,//在这里使用vuex的store
  el: '#app',
  render: h => h(App)
});


