import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
    strict:true,//在严格模式下,不允许非mutation操作vuex中store管理的数据
    state:{//state中存储数据
        products:[
            {name:"马云",price:200},
            {name:"马化腾",price:140},
            {name:"马冬梅",price:20},
            {name:"马蓉",price:10},
          ]
    },
    getters:{//getters中获取数据(只读不写)
        saleProducts:(state)=>{
            //获取打折后的商品价格,这里用map遍历其中的每个prodcut返回price折半的结果
            var saleProducts = state.products.map(
              product => {
                return {
                  name :"*"+product.name,
                  price: product.price/2
                }
            });
            //将打折的商品数组返回
            return saleProducts;
        }
    },
    mutations:{//mutations中注册改变state的事件(只写不读)
        reducePrice:(state,payload)=>{
            //如果在这里用setTimeout,那么就是在调用reducePrice之后,3秒后再执行商品降价的操作
            // setTimeout(function(){
                //商品降价:遍历store中的每个商品使其price减少
                state.products.forEach(product => {
                    product.price -= payload;
                });
            // },3000);
        }
    },
    actions:{//actions用于提交mutation,可以实现异步操作
        reducePrice:(context,payload)=>{
            //在这种方式下,数据改变和mutation的方法执行是同时的
            setTimeout(function(){
                context.commit("reducePrice",payload);
            },3000);
        }   
    }
});

